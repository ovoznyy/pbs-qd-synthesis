# 2 layers, scalign 1:5

## 20, 20 - 662 params - my old model 
### Trained on full data (2315+237)
#### No regularization
- Full -6.52   -6.68  -6.74
- Test -5.83   -6.1   -6.23
#### Drop 0, L2=1e-6, batch 256 - time 9:43
- Full -6.47   -6.54
- Test -5.97   -6.14
#### Drop 10%, L2=1e-6, batch 256 - time 11:00 
- Full -5.74
- Test -5.19

### Train on old (2315/2), validation 50% (30k epochs, no batches)
- Drop 5%
    - L2 1e-8 - 6.72 5.6 overfits after 7k
    - L2 1e-7 - 6.64 5.62 overfits after 7k
    - L2 1e-6 - 6.5  5.65
    - L2 1e-5 - 5.95 5.57?
    - L2 1e-4 - 5.7  5.4?

### Trained on old (2315), test on new (237) (no batches)
- Drop 0 
    - L2 0      2.9 overfit
    - L2 1e-7 - 3.5 and then overfits
    - L2 1e-5 - 3.5 then overfits
    - L2 1e-4 - 3.3 and slowly goes up. train -5.9
    - L2 1e-3 - 3.3 and stops, train also stops at -5.6
- Drop 5%
    - L2 1e-8  6.37 4.5 in 10k, and then went to 4.2
    - L2 3e-8  6.37 4.35 in 10k then overfits to 4.3
    - **L2 1e-7  6.3  4.3**
    - L2 1e-6  6.16 4.1 and test keeps improving
    - L2 1e-5  5.87 3.5
- Drop 10%
    - L2 1e-8 

###Train on new (237), test on old (2315)
- Drop 5%     old       new
    - L2 1e-8 5.25->5.2 slow overfit, train(new) 5.9
    - L2 1e-7 5.2->5.1  slow overfit, 5.84
    - **L2 1e-6 5.3       5.6-5.7**
    - L2 1e-5 4.9       5.1

### Trained on old+half new (2315+237/2), test on new (237/2) (no batches) split 0.046
- Drop 5%       train, test
    - L2 1e-8   6.2    5.3->5.26 slow overfit - very decent training
    - L2 1e-8   6.1    5.3 early stopping 10k epochs
    - L2 1e-7   6.12   5.27 - very decent training in/out plot
    - L2 3e-7   6.1    5.18->5.15 after 16k
    - **L2 1e-6 5.9    4.9**  
    
- Drop 5%, batch 10 - very slow
    - L2 1e-8  6.37    4.9 couldn't finish
    - L2 1e-7  6.2     5.1->4.96 overfit after 5k epochs 

- Drop 10%      train, test
    - L2 1e-8   6.0    5.18->5.09  slow overfit, decent training
    - L2 1e-7   5.97   5.22->5.2   slow overfit?
    - **L2 1e-6 5.8    5           no overfit,   not good???**




## 50,50 - 3152 params
### Trained on full data, test on new data (every 15k epochs)
- Full -6.55  -6.74  -6.83
- Test -5.87  -6.33  -6.62






# 3 layers
## 10, 10, 10
### Trained on full data, test on new data (every 15k epochs)
- Full .....  -6.5  -6.57
- Test .....  -5.9  -5.95




## 20, 20, 20 - 1082 params
### Trained on full data, test on new data (every 15k epochs)
#### No regularization
- Full -6.6   -6.8  -6.87
- Test -5.95  -6.3  -6.6
#### Drop 10%, L2=1e-6, batch 256 - time 13:44
- Full -5.9
- Test -5.5

### Trained on old+half new (2315+237/2), test on new (237/2) (batch 1300) split 0.046
- Drop 5%
    - L2 1e-6  6.1   5.25-5.15 slowly overfits
    - L2 1e-5  5.7   4.7-4.6 after 8k epochs   





# 4 layers

## 10, 10, 10, 10 - 452 params
### Trained on full data, test on new data (every 15k epochs)
- Full -6.53 -6.6
- Test -5.85 -6.0  


## 30, 30, 30, 30 - 3152 params
### Trained on full data, test on new data (every 15k epochs)
- Full -6.83  -7.14   **-7.3**
- Test -6.5   -7.53   **-7.86**  