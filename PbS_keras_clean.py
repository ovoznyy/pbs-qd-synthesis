########################################################################
# PbS QD synthesis from PbOA2 and TMS
########################################################################
# Sargent Lab, ECE, University of Toronto, ted.sargent@utoronto.ca
# Data analysis: Oleksandr Voznyy,  o.voznyy@utoronto.ca
# Experimental data points:  Larissa Levina, James Fan
# Model development: Oleksahdr Voznyy, Ankit Jain, Misha Askerka
# Code cleanup: Daniel Voznyy
# The most up to date code is available at https://gitlab.com/ovoznyy/pbs-qd-synthesis/
########################################################################

import time

import keras
import matplotlib.pyplot as plt
import numpy as np
from keras import backend as K
from keras import layers
from keras.layers.core import Lambda

########################################################################
# Parameters
########################################################################

TRAIN = False
MODEL_NAME='./weights/my_model'

SCALING = 3  # p2v vs wavelength

N1 = 20  # nodes in 1st hidden layer
N2 = 20
# N3 = 50
# N4 = 50

L2 = 2e-6
DROP_VISIBLE = 0.0 # on 9 inputs
DROP = 0.1
DROP_PREDICT = 0.1

EPOCHS = 15000  # ~10k is usually ok, more might be required to fit 2000nm better ????
BATCH = 255  # total data 2552
SPLIT = 0.046 # old 2315 + new 237 = 2552 example of validation on half new data

if not TRAIN:
  DROP = DROP_PREDICT

########################################################################
# Functions
########################################################################
def plot_history(history):
    plt.figure()
    plt.xlabel('Epoch')
    plt.ylabel('Error')
    plt.plot(history.epoch, np.log(history.history['loss']), label='Train Loss')
    plt.plot(history.epoch, np.log(history.history['val_loss']), label='Val loss')
    plt.legend()
    # plt.ylim([0, 1])
    plt.show()


########################################################################
# Read data 
########################################################################

f = open("full.dat", "r")  # cross-validation will be picked from the same data

x_train = []
y_train = []

x_validation = []
y_validation = []

x_noise = []
y_noise = []

x_test = []
y_test = []

for line in f:
    val = line.split()
    # Values: t_env,  Pb,  ODE,  OLA,  tmax,  TMS,  ODE,  Cl,  Cl
    # TODO read using Pandas from full CSV with column names, drop unnecessary columns
    x = [float(val[i]) for i in range(9)]
    E = 1240 / float(val[9])
    peak2valley = float(val[10])
    y = [E, (0.16 * np.exp(-peak2valley / 3.07) + 0.04)]

    # Recognizes "old" pre-ML data (Pb = 18, 19, 4.5, 7) to use for training, all the new data will be for testing
    if (float(val[1]) == 18) or (float(val[1]) == 4.5) or (float(val[1]) == 7) or (float(val[1]) == 19):
        x_train.append(x)  # x_train is pre-ML old data
        y_train.append(y)
    else:
        x_test.append(x)  # x_test is data from new experiments performed to test ML predictions
        y_test.append(y)


    # Most popular recipe for 950nm:
    # 4.5      15  0  125-127-130C  180 / 10     0  0-0.25-0.33
    if (((float)(val[1])==4.5)  and  ((float)(val[2])==15)   # Pb
      and((float)(val[3])==0)                                # OLA
      and((float)(val[4])>124)  and  ((float)(val[4])<131)   # temperature
      and((float)(val[5])==180)                              # TMS
      and((float)(val[6])>8)    and  ((float)(val[6])<12)    # ODE2
      and((float)(val[7])==0)   and  ((float)(val[8])==0) ): # Cl

      x_noise.append(x)  # data used to plot the degree of noise for nominally identical input parameters
      y_noise.append(y)

    '''
    # Best recipe for 950nm:
      # 17.4,  15, 0.4,  109, 206, 9.6, 0, 0.3
    if (((float)(val[1]) == 17.4) and ((float)(val[2]) == 15)  # Pb
              and ((float)(val[3]) == 0.4)                       # OLA
              and ((float)(val[4]) ==109)                        # temperature
              and ((float)(val[5]) == 206)                       # TMS
              and ((float)(val[6]) == 9.6)                       # ODE2
              #and ((float)(val[7]) == 0) and ((float)(val[8]) == 0.3) # Cl
              ):

      x_noise.append(x)  # data used to plot the degree of noise for nominally identical input parameters
      y_noise.append(y)
    '''

X = np.array(x_train + x_validation + x_test) # full data, used to determine min/max
Y = np.array(y_train + y_validation + y_test)

# x_train is pre-ML data, x_test is post-ML data.
# Append the test data at the end. Will use Keras validation_split to separate them later
x_train = np.array(x_train + x_test)
y_train = np.array(y_train + y_test)

# x_validation not used because cross-validation is done using Keras validation_split, or directly with x_test
# thus can use x_validation for plotting lines (see plots below)
x = [15, 4.5, 15, 0, 127, 180, 10, 0, 0]  # 950nm, 2.5 - see lines.dat for more examples
#x = [15, 17.4,15, 0.4,109, 206, 9.6, 0, 0.3]    # 950nm, 4.5 best
y = [0, 0]  # these values are not used, need them only to maintain the same dimension of y as x
x_validation.append(x)  # TODO rename to x_lines
y_validation.append(y)
x_validation = np.array(x_validation)
y_validation = np.array(y_validation)

x_test = np.array(x_test)
y_test = np.array(y_test)

#x_test = np.array(x_noise)
#y_test = np.array(y_noise)

print('Data size: train, val, test, full:', len(x_train), len(x_validation), len(x_test), len(X))

# Normalize -----------------------------------------------------------

# might need to use some predefine constant min max, so that adding new points would not change all the scaling
# see min(tensor) to write this even shorter
max_x = [np.max(X[:, i]) for i in range(9)]
min_x = [np.min(X[:, i]) for i in range(9)]

max_y = [np.max(Y[:, i]) for i in range(2)]
min_y = [np.min(Y[:, i]) for i in range(2)]

print('X range:')
print(min_x)
print(max_x)

print('Y range:')
print(min_y)
print(max_y)

scaling_y = [1, SCALING]  # because Keras cannot apply different weights to 2 different output values in the same layer

for i in range(len(X)):
    for j in range(9):
        if i < len(x_train):
            x_train[i, j] = (x_train[i, j] - min_x[j]) / (max_x[j] - min_x[j])
        if i < len(x_validation):
            x_validation[i, j] = (x_validation[i, j] - min_x[j]) / (max_x[j] - min_x[j])
        if i < len(x_test):
            x_test[i, j] = (x_test[i, j] - min_x[j]) / (max_x[j] - min_x[j])

    for j in range(2):
        if i < len(x_train):
            y_train[i, j] = (y_train[i, j] - min_y[j]) / (max_y[j] - min_y[j]) / scaling_y[j]
        if i < len(x_validation):
            y_validation[i, j] = (y_validation[i, j] - min_y[j]) / (max_y[j] - min_y[j]) / scaling_y[j]
        if i < len(x_test):
            y_test[i, j] = (y_test[i, j] - min_y[j]) / (max_y[j] - min_y[j]) / scaling_y[j]

########################################################################
# Keras model and training
########################################################################
start_time = time.time()

model = keras.Sequential([
    #layers.Dropout(DROP_VISIBLE, input_shape=(9,)),
    #layers.Dense(N1, activation='elu', kernel_regularizer=keras.regularizers.l2(L2)),
    layers.Dense(N1, activation='elu', kernel_regularizer=keras.regularizers.l2(L2), input_dim=9),
    # layers.Dropout(DROP),
    Lambda(lambda drop: K.dropout(drop, level=DROP)),
    layers.Dense(N2, activation='elu', kernel_regularizer=keras.regularizers.l2(L2)),
    # layers.Dropout(DROP),
    Lambda(lambda drop: K.dropout(drop, level=DROP)),
    # layers.Dense(N3, activation='elu', kernel_regularizer=keras.regularizers.l2(L2)),
    # layers.Dropout(DROP),
    # layers.Dense(N4, activation='elu', kernel_regularizer=keras.regularizers.l2(L2)),
    # layers.Dropout(DROP),
    layers.Dense(2)
])

model.compile(optimizer='Adam',
              loss=['mse'],
              metrics=['mse'])

model.summary()

# ---------------------------------------------------------------------------
if TRAIN:
    #model.load_weights(MODEL_NAME)  # in case you want to restart training from previous

    history = model.fit(x_train, y_train, epochs=EPOCHS, verbose=2, batch_size=BATCH,
                        # validation_data=(x_test, y_test))
                        validation_split=SPLIT)
    model.save_weights(MODEL_NAME)

    runtime = time.time() - start_time
    print("Total time " + str(runtime) + " time per epoch " + str(runtime/EPOCHS))

    plot_history(history)
    exit()

else:
    model.load_weights(MODEL_NAME)

#######################################################################################################
# Plot predictions vs. true
#######################################################################################################

y_predict = model.predict(x_train)

plt.plot(y_train[:, 0], y_predict[:, 0], '.c', label='bandgap')

#y_predict = model.predict(x_test)
#plt.plot(y_test[:, 0], y_predict[:, 0], '.r')  # to show experimental noise

plt.legend()
plt.show()

plt.plot(y_train[:, 1], y_predict[:, 1], '.b', label='p2v')
plt.legend()
plt.show()

#################################
y_predict = model.predict(x_test)

plt.plot(y_test[:, 0], y_predict[:, 0], '.c', label='bandgap')
plt.legend()
plt.show()

plt.plot(y_test[:, 1], y_predict[:, 1], '.b', label='p2v')
plt.legend()
plt.show()

'''
#CdCl effect
plt.plot(x_data[:,5], y_data[:,1], '.g')
plt.show()

plt.plot(x_data[:,6], y_data[:,1], '.g')
plt.show()
'''

##########################################################################
# Plot training and testing 2D data graphs
###########################################################################

Y_pred = model.predict(x_train)
plt.plot(y_train[:, 0], y_train[:, 1], 'or', Y_pred[:, 0], Y_pred[:, 1], 'ob', alpha=0.2)
plt.show()

Y_pred = model.predict(x_test)
plt.plot(y_test[:, 0], y_test[:, 1], 'oy', Y_pred[:, 0], Y_pred[:, 1], '.g')
# plt.plot(y_test[:,0], y_test[:,1], 'oy', y_test[:,0], Y_pred[:,1], '.g')
plt.show()

##############################################################################
# Scan lines along one dimension
##############################################################################

color = ['blue', 'black', 'white', 'cyan', 'magenta', 'yellow', 'grey', 'green', 'green']
errorbar = [0.002, 0.06, 0.014, 0.002, 0.05, 0.036, 0.014, 0.016, 0.032]

print(x_validation)
for k in range(len(x_validation)):

    plt.plot(y_train[:, 0], y_train[:, 1], 'or')

    for j in range(9):  # number of input parameters along which to plot a line
        x_lines = []
        y_lines = []

        for i in range(3000):  # points along the line
            # load the needed point in parameter space
            x_point = list(x_validation[k, :])
            # now change one of the input parameters to scan its full range of possible values
            x_point[j] = i / 3000  # full range
            # x_point[j] = x_point[j] + (0.5 - i/1000) * errorbar[j] *2   # error bar
            y_point = [0, 0]

            x_lines.append(x_point)
            y_lines.append(y_point)

        x_lines = np.array(x_lines)
        y_lines = np.array(y_lines)

        Y_lines = model.predict(x_lines)
        plt.plot(Y_lines[:, 0], Y_lines[:, 1], 'o', color=color[j], alpha=0.1)

        #plt.plot(y_test[:, 0], y_test[:, 1], 'o', markerfacecolor='cyan', markeredgecolor='cyan', alpha=0.9)

    plt.show()

#########################################################################
#
# Print the worst-fit points in order to repeat them experimentally
#
#########################################################################
'''
print('Calculating the worst points')

x_worst = x_test
y_worst = y_test
#use test_data which actually read the full data, but did not repeat it for balancing and not augment it
Y_pred = sess.run(prediction, feed_dict={x: x_test, y_: y_test, keep_prob: 1})




file = open("worst-fit points.txt","w")

for i in range(len(x_worst)):
  # if wavelength is very wrong
  if(abs(y_worst[i,0]-Y_pred[i,0])>0.2):
    print (i,y_worst[i,0],y_worst[i,1])  
    #scale back
    for j in range(9):
      x_worst[i,j] = x_worst[i,j] * (max_x[j] - min_x[j]) + min_x[j]
    for j in range(2):
      y_worst[i,j] = y_worst[i,j] * (max_y[j] - min_y[j]) + min_y[j]
    #convert to human-readable
    y_worst[i,0] = 1240/y_worst[i,0]
    y_worst[i,1] = y_worst[i,1]/80*(y_worst[i,0]-450)   *2

    file.write(str(x_worst[i,:])+str(y_worst[i,:])+'\n'.format('f'))
file.close()


'''

############################################################################################
#
# Predictions 2D map, neat vs. OLA vs. CdCl2
#
############################################################################################

print('Generating predictions')

x_pred = []
x1_pred = []
x3_pred = []
y_pred = []

for i in range(50000):
    # this is in units after normalization!
    #                            t_env                      Pb                               ODE                         OLA                                           t                          TMS                          ODE                         Cl                      Cl
    # full range
    a_pred = [np.random.uniform(0.5, 0.5), np.random.uniform(0, 1.), np.random.uniform(0.273, 0.273),
              np.random.uniform(0.0, 0.5), np.random.uniform(-0.1, 1.1), np.random.uniform(0, 1.),
              np.random.uniform(0., 1.), np.random.uniform(0, 0.1), np.random.uniform(0, 0.1)]
    # a_pred = [np.random.uniform(0.3,0.3), np.random.uniform(-0.31,1.5),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.5),                np.random.uniform(-0.1,1.1),      np.random.uniform(-0.217,1.5), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.2), np.random.uniform(0,0.2)]
    # a_pred = [np.random.uniform(0.9,0.9), np.random.uniform(0.,1.),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,1),                np.random.uniform(0,1),      np.random.uniform(0.,1.), np.random.uniform(0.,1.), np.random.uniform(0,0.), np.random.uniform(0,0.)]

    # double range
    # a_pred = [np.random.uniform(0.9, 0.9), np.random.uniform(0.8, 1.5), np.random.uniform(0.273, 0.273),
    #           np.random.uniform(0.0, 0.15), np.random.uniform(0., 1), np.random.uniform(+0.217, 2),
    #           np.random.uniform(-0.1, 1.1), np.random.uniform(0.0, 0.0), np.random.uniform(0.05, 0.1)]

    '''
    # Pb:S = const 
    pb = np.random.uniform(0.+0.31,2.5+0.31)
    #pb = np.random.uniform(0.7+0.31,1.3+0.31)
    s  = np.random.uniform(-0.117+0.217,1.5+0.217)
    #s = (pb + np.random.uniform(-0.4,0.4))/ 1.87 + 0.217  # with OLA
    #s = (pb + np.random.uniform(-0.5,0.5))/ 0.7 + 0.217  # with OLA
    #s = (pb + np.random.uniform(-0.1,0.1)) /  1.75    # without OLA
    
    # 600nm
    #a_pred = [np.random.uniform(0.3/50,0.3/50), pb - 0.31,                    np.random.uniform(0.273,0.273),   np.random.uniform(0.5,0.7),                np.random.uniform(-0.1,0.1),      s-0.217,                       np.random.uniform(-0.1,1.1), np.random.uniform(0,0.), np.random.uniform(0,0.)]         
    # full range
    #a_pred = [0.3/50,                                 pb - 0.31,              np.random.uniform(0.273,0.273),   np.random.uniform(0.,0.2),                np.random.uniform(-0.1,1.1),      s-0.217,                       np.random.uniform(-0.1,1.1), np.random.uniform(0,0.), np.random.uniform(0,0.)]            
    a_pred = [0.9/50,                                  pb - 0.31,                          0.273,                np.random.uniform(0.,0.1),                np.random.uniform(0.2,0.8),      s-0.217,                      np.random.uniform(0.,1), np.random.uniform(0,0.), np.random.uniform(0,0.1)]      
    
    #optimal for 600nm, OLA 
    #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(0.45,1.2),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.2),                np.random.uniform(-0.05,0.25),      np.random.uniform(0.8,1.15), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.2), np.random.uniform(0,0.2)]   
    
    #optimal for 950nm, Pb 10, no OLA, no Cd
    #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(0.3,0.5),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.),                np.random.uniform(0.35,0.6),      np.random.uniform(0.4,0.6), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.), np.random.uniform(0,0.)]      
    #optimal for 950nm, Pb 18, OLA
    #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(0.8,1.),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.3),                np.random.uniform(0.5,0.7),      np.random.uniform(0.4,0.6), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.15), np.random.uniform(0,0.15)]
    #optimal for 950nm, Pb 11, OLA
    #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(0.4,0.6),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.3),                np.random.uniform(0.4,0.8),      np.random.uniform(0.2,0.4), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.15), np.random.uniform(0,0.15)]   
    
    #optimal for 1170nm, Pb 10, no OLA, no Cd
    #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(0.6,0.9),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.),                np.random.uniform(0.5,0.65),      np.random.uniform(0.35,0.65), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.), np.random.uniform(0,0.)]      
    #optimal for 1500nm, noOLA
    #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(1.1,1.6),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.),                np.random.uniform(0.5,0.75),      np.random.uniform(0.65,0.95), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.), np.random.uniform(0,0.)]      
    #optimal for 2200nm, noOLA   
    #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(2.2,3),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.),                np.random.uniform(0.2,0.7),      np.random.uniform(1.1,1.5), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.), np.random.uniform(0,0.)]   
    '''
    x_pred.append(a_pred)  # gray with Cl and OLA

    # same points with only one parameter changed ====================================
    b_pred = list(a_pred)
    d_pred = list(a_pred)

    # CdCl
    b_pred[7] = np.random.uniform(0., 0.)  # blue     OLA, no Cl
    b_pred[8] = np.random.uniform(0., 0.)  # blue

    d_pred[7] = np.random.uniform(0., 0.)  # green no OLA, no Cl
    d_pred[8] = np.random.uniform(0., 0.)  # green

    # season
    # b_pred[0] = np.random.uniform(0.9/50,0.9/50)  #blue
    # d_pred[0] = np.random.uniform(0.9/50,0.9/50)  #green

    # OLA
    b_pred[3] = np.random.uniform(0.0, 0.1)  # blue     OLA, no Cl  (used for final predictions later on)
    d_pred[3] = np.random.uniform(0., 0.)  # green no OLA, no Cl

    # 18 vs 4.5
    # b_pred[1] = np.random.uniform(0.,1)

    # ODE
    # b_pred[3] = np.random.uniform(0.35,0.65)

    x1_pred.append(b_pred)  # blue
    x3_pred.append(d_pred)  # green

    # placeholder for results ========================================================
    # not needed with Keras
    # c_pred = [0, 0]
    # y_pred.append(c_pred)

x_pred = np.array(x_pred)
x1_pred = np.array(x1_pred)
x3_pred = np.array(x3_pred)
# y_pred = np.array(y_pred) # not needed with Keras


# full range (including OLA and CdCl2)
Y_pred = model.predict(x_pred)
plt.plot(Y_pred[:, 0], Y_pred[:, 1], 'o', markerfacecolor='silver', markeredgecolor='silver', alpha=0.7)

# OLA, no Cd, one season
Y1_pred = model.predict(x1_pred)
plt.plot(Y1_pred[:, 0], Y1_pred[:, 1], 'o', markerfacecolor='blue', markeredgecolor='blue', alpha=0.5)

# no OLA, no Cd, one season
Y3_pred = model.predict(x3_pred)
plt.plot(Y3_pred[:, 0], Y3_pred[:, 1], 'og', alpha=0.5)

# original data
plt.plot(y_train[:, 0], y_train[:, 1], 'or')  # all
plt.plot(y_test[:, 0],  y_test[:, 1], 'o', markerfacecolor='white', markeredgecolor='red')  # with Cl


plt.show()



#########################################################################
# UNCERTAINTIES
#########################################################################
'''
for i in range(200):
  Y_pred = (sess.run(prediction, feed_dict={x: x_pred, y_: y_pred, keep_prob: 0.99}))
  plt.plot(Y_pred[:,0], Y_pred[:,1], '.b')

Y_pred = (sess.run(prediction, feed_dict={x: x_pred, y_: y_pred, keep_prob: 1}))
plt.plot(Y_pred[:,0], Y_pred[:,1], 'or')

plt.show()
'''

# x_pred = x_test # for sigopt
# x_pred = x_data  # original data
##########################################################################
# Plot inputs distribution
###########################################################################
'''

Y_pred = (sess.run(prediction, feed_dict={x: x_pred, y_: y_pred, keep_prob: 1}))   # full range predictions

# define how close the point is to optimum
size=[]
wavelength=[]
p2v=[]
for i in range(len(x_pred)) :
  #size.append (  0.8*(Y_pred[i,0]-0.527)**2 + 0.2* (1e-1)**2  ) # only wavelength
  #size.append (  0.8*(Y_pred[i,0]-1.035)**2  +  0.2*(Y_pred[i,1]-0.6)**2  )    # both, 600 nm     
  #size.append (  0.8*(Y_pred[i,0]-0.527)**2  +  0.2*(Y_pred[i,1]-0.225)**2  )   # both, 950 nm 
  #size.append (  Y_pred[i,1] )   # p2v 950 nm   
  #size.append (  0.8*(Y_pred[i,0]-0.427)**2  +  0.2*(Y_pred[i,1]-0.043)**2  )   # both, 1070 nm   
  #size.append (  0.8*(Y_pred[i,0]-0.359)**2  +  0.2*(Y_pred[i,1]-0.004)**2  )   # both, 1170 nm     
  #size.append (  0.8*(Y_pred[i,0]-0.199)**2  +  0.2*(Y_pred[i,1]-(-0.025))**2  )# both, 1500nm
  #size.append (  0.8*(Y_pred[i,0]-0.)**2      +  0.2*(Y_pred[i,1]-0.318)**2  )   # both, 2200nm    
  
  if abs(Y_pred[i,0]-0.2)>0.01:  # slice through
    size.append (  0  )
  if abs(Y_pred[i,0]-0.2)<=0.01:
    size.append (  Y_pred[i,1]  )

  wavelength.append(Y_pred[i,0])
  p2v.append(Y_pred[i,1])
  #wavelength.append(0.527) #950nm
  
size = np.array(size)
p2v=np.array(p2v)
wavelength=np.array(wavelength)




# 2D plot Pb vs S plane, deviation from optimum ==================================================================================
#plt.scatter(x_pred[:,1],x_pred[:,5], s=(1/size)**2/30000, c=1/size)  # deviation
plt.scatter(x_pred[:,1],x_pred[:,5], s=size*10, c=size)               # p2v for a slice
#plt.scatter(x_pred[:,1],x_pred[:,4], s=(1/size), c=1/size)           # abs value
plt.xlabel("Pb")
plt.ylabel("S")
plt.show() 
'''

'''
# 2D plot Pb vs S plane, wavelength
plt.scatter(x_pred[:,1],x_pred[:,5], s=wavelength, c=wavelength, cmap=cm.nipy_spectral)  # wavelength
plt.colorbar()
plt.xlabel("Pb")
plt.ylabel("S")
plt.show() 

# 2D plot Pb vs S plane, peak2valley
plt.scatter(x_pred[:,1],x_pred[:,5], s=p2v, c=p2v)  # p2v
plt.colorbar()
plt.xlabel("Pb")
plt.ylabel("S")
plt.show() 
'''

'''
# 3D ==================================================================================
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x_pred[:,1], x_pred[:,5],  x_pred[:,4], zdir='z', s=(1/size)**2/30000, c=1/size, depthshade=False)
ax.set_xlabel('Pb')
ax.set_ylabel('S')
ax.set_zlabel('t_max')
plt.show() 
'''

# 2D along Pb-S line vs t ==================================================================================
'''
#plt.scatter((x_pred[:,1]+0.31)/(x_pred[:,5]+0.217),x_pred[:,4], s=(1/size)**2/30000, c=1/size)  
plt.scatter((x_pred[:,1]+0.31)/(x_pred[:,5]+0.217),x_pred[:,4], s=size*10, c=size)  # slice
plt.xlabel("Pb/S")
plt.ylabel("t")
plt.show() 
'''

'''
# 2D along Pb-S line vs OLA
plt.scatter((x_pred[:,1]+0.31)/(x_pred[:,5]+0.217),x_pred[:,3], s=(1/size)**2/30000, c=1/size)  
plt.xlabel("Pb/S")
plt.ylabel("OLA")
plt.show() 


# 2D plot Pb/S vs t, wavelength
plt.scatter((x_pred[:,1]+0.31)/(x_pred[:,5]+0.217),x_pred[:,4], s=wavelength, c=wavelength, cmap=cm.nipy_spectral)  # wavelength t
plt.colorbar()
plt.xlabel("Pb/S")
plt.ylabel("t")
plt.show() 

# 2D plot Pb/S vs t, p2v
plt.scatter((x_pred[:,1]+0.31)/(x_pred[:,5]+0.217),x_pred[:,4], s=p2v, c=p2v, cmap=cm.nipy_spectral)  # p2v t
plt.colorbar()
plt.xlabel("Pb/S")
plt.ylabel("t")
plt.show() 
'''
'''
# 2D plot Pb/S vs OLA plane, wavelength
plt.scatter((x_pred[:,1]+0.31)/(x_pred[:,5]+0.217),x_pred[:,3], s=wavelength, c=wavelength)  # wavelength OLA
plt.colorbar()
plt.xlabel("Pb/S")
# plt.ylabel("OLA")
plt.show() 
'''

# exit()

# x1_pred = x_pred  # for sigopt
# x1_pred = x_test  # for James
#########################################################################
#
# Scale predictions back
#
#########################################################################
print('Scaling predictions back ')

for i in range(len(x1_pred)):  # x1 = b = blue = OLA
    for j in range(9):
        # if j == 0:
        #     x1_pred[i, j] = x1_pred[i, j] * 50 #t_env is not scaled in this version
        x1_pred[i, j] = x1_pred[i, j] * (max_x[j] - min_x[j]) + min_x[j]
    Y1_pred[i, 0] = Y1_pred[i, 0] * (max_y[0] - min_y[0]) + min_y[0]
    Y1_pred[i, 1] = SCALING * Y1_pred[i, 1] * (max_y[1] - min_y[1]) + min_y[1]

    # Y1 = FWHM/E = 0.16*np.exp(-peak2valley/3.07)+0.04
    # p2v= -ln[(Y1-0.04)/ 0.16]*3.07
    Y1_pred[i, 0] = 1240 / Y1_pred[i, 0]
    Y1_pred[i, 1] = -np.log((Y1_pred[i, 1] - 0.04) / 0.16) * 3.07

plt.plot(Y1_pred[:, 0], Y1_pred[:, 1], '.b', alpha=0.1)

print('Showing the red experimental data rescaled back')
for i in range(len(x_train)):
    for j in range(9):
        # if(j==0):
        #  x_train[i,j]=x_train[i,j]*50  # no need to scale here because it is not plotted
        x_train[i, j] = x_train[i, j] * (max_x[j] - min_x[j]) + min_x[j]

    y_train[i, 0] = y_train[i, 0] * (max_y[0] - min_y[0]) + min_y[0]
    y_train[i, 1] = SCALING * y_train[i, 1] * (max_y[1] - min_y[1]) + min_y[1]

    # Y1 = FWHM/E = 0.16*np.exp(-peak2valley/3.07)+0.04
    # p2v= -ln[(Y1-0.04)/ 0.16]*3.07
    y_train[i, 0] = 1240 / y_train[i, 0]
    y_train[i, 1] = -np.log((y_train[i, 1] - 0.04) / 0.16) * 3.07

plt.plot(y_train[:, 0], y_train[:, 1], '.r')
plt.show()

########################################################################
#
# Show the best
#
########################################################################
print('Filtering to required range')

x_cut = []
y_cut = []

for i in range(len(x1_pred)):
    if ((Y1_pred[i, 0] > 945) and (Y1_pred[i, 0] < 955) and (Y1_pred[i, 1] > 4.1) and (Y1_pred[i, 1] < 40)):
        x_cut.append(x1_pred[i, :])
        y_cut.append(Y1_pred[i, :])

x_cut = np.array(x_cut)
y_cut = np.array(y_cut)

plt.plot(y_cut[:, 0], y_cut[:, 1], '.y')
plt.show()

##########################################################################

file = open("950nm_predictions.txt", "w")
file.write(str(x_cut))
# f.write('%8.2f' % (str(x_cut))
file.write(str(y_cut))
file.close()
