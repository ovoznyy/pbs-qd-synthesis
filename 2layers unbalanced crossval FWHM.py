########################################################################
# November 1, 2017
########################################################################
# 3 layers * 20 nodes 
# Weigths are initialized with values that immediately minimize the error
# Data is augmeneted by copying Cl points to non-Cl and manually changing the 2pv, and reverse too
# Strong L2 is used
# y-axis rescaled by 2, and 1*err_nm + 1*err_p2v is used, to provide equal constraint on nm and p2v
# Training and testing data are mixed into one full dataset (thanks to L2)
# Data is balanced by increasing the number of entries for rare datapoints (17000 total, true set ~2300)
# 650nm-0.5p2v points are not rebalanced to reduce their error contribution
# regularize better by training on batches of ~10-1000
# 5% dropout (at the expense of larger net: 3*80 nodes)


########################################################################
# Dec 5, 2017
########################################################################
# Rescaled t_env 10.000X
# Increased dropout to 15%, increased net to 160*80*40, made funnel to make it faster, decreased L2 to 3e-4
# Use elu, don't know if it matters
# Best result: R2 = 96.47%, 71.8% Curves too rough



########################################################################
# Jan 2018
########################################################################
# Return to 2 layers, 20x20, no dropout
# L2 2e-2 (started low and then gradually increased); R2 = 94.5%,  65.3% (on full data) Smoothest possible curves - yeah, R2 is worse, but is the fit really worse than 3 layers???
# L2 2-e-2 (from the very beginning)
# L2 2e-3; R2 = 95.1%, 68.16% (starts very smooth, ends not too bad, far extrapolation is bad)
# L2 2e-4; R2 = 93.4%, 65.58% (t_env goes crazy)
########################################################################
# 1/p2v, rescaling the quality for 950 vs 1350 using a sin
# switch again to distance**2 error, L2 2e-2 is too strict (620 OLA tail is not reproduced)
# L2 2e-3 (620 tail is OK) R2 = 94.54, 57.42 after short (95.3, 59.1 after long optimization)
# L2 3e-3 R2 = 95.17, 58.6 (after short)
#
# L2 1e-5 with dist**4 R2 = 95.6, 60.16 (a bit too curvy)
# L2 3e-5 with dist**4 R2 = 
#
########################################################################
# Cross-validation 1/p2v*sin
########################################################################
# L2   validation  5X (error 2*er+1*err)**2 200k steps
# 5e-4   80.2    56    (two really bad ones included - too unconstrained)
# 1e-3   89.96   59.24
# 2e-3   90.65   55.9
# 3e-3   91.06   54.8 - had one particularly bad partition (not included in average)



########################################################################
# Feb 10, 2018
########################################################################
# Cross-validation FWHM =  (0.16*exp(-p2v/3.07)+0.04)*E
########################################################################
# 2*10 nodes
# L2   validation  6X (error 2*er+1*err)**2 200k steps, 
# 5e-4  81.6+-20  72+-25
# 1e-3  90+-2     84.4+-2.5; another run 91+-0.9  85.4+-1.9
# 2e-3  87.2+-5   80.1+-8.7
# 3e-3  89.4+-2.6 81.3+-4.2

########################################################################
# Cross-validate on ML vs non-ML data (same for all runs) FWHM/E
# This is not very correct, as true local variations can be large, 
# but we just got lucky to extrapolate a little better when forced it to be too smooth
########################################################################
# 2*20 nodes, L2, 200k steps first run and then restart with 100k steaps
# batch 500????
#            nm    |    p2v  - fit independently
# 5e-4  94.8  37   |
# 1e-3  94.5  48   | 
# 2e-3  94.4  49   |  82    -79  (not too curvy if nm is not fit simultaneously)
# 4e-3  93.8  61.5 |  79    -32
#!6e-3  92.86 85.34|  81.38  69.19  FITTED WITH 1+1 error
#!8e-3  92    83   |  79.8   67.8   FITTED WITH 1+1 error
# 8e-3  93.1  56   |  80    -22
# 2e-2             |  76    -22, clearly overconstrained (2 bands 4.5 and 18 clearly visible), but overall is actually not bad
# 4e-2             |
# 8e-2             |  67    -57
# 2e-1             |  46    -108 just 10k steps 
# 8e-1             |  40    -85
#
########################################################################
# Feb 17
# FWHM/E, 20*2, L2 
# Dropout 75%  !!!!!!!!!!!
# (the model is capable, it just relies on less lines per fit, thus higher W, lower L2)
# Cross-validate on old data 80/20, test predictions on ML data as test,  400k steps, 4 runs each
# batch 100
########################################################################
#            nm          |    p2v/1            |   p2v/10    - fit independently
# L2   train, val, test  |  1813  453  130 (data till Jan 2018)
#
# 5e-4  93.6  91.7  85.2 |  ML test included in training, stupid
# 1e-3  93.1  91.4  86   |  ML test included in training
# 2e-3  90.4  91.2  78   |  83.7   79.5  ML test included in training, not better than well trained combined nm+p2v?
#
# 5e-4  92.5  90    21   | 86    80   -18
# 1e-3  93.8  93.5  50.4 | 85.8  81    -6
# 2e-3  93.2  92.1  53.5 | 84.6  84    23  <==== looks best cross-val
# 2e-3                   | 84.9  83   -76  batch 1000, 300k steps
# 2e-3                   | 83.9  81    -9  batch 10,   400k, converges, then quickly diverges, then slowly converges
# 2e-3                   | 84.9  82.9  14  batch 10,  4000k steps
# 2e-3                   | 84    84    11  without dropout not as good
#2.5e-3 93.3  93.4  54.5 | 80.9  79.8  20  fit together is NOT much better for extrapolation (within the error bar)
#2.5e-3 94          54   | 82           0  gets significantly worse after more than 400k steps. i.e. worth constraining it more (more dropout, or more L2) 
#2.5e-3 93          88.5 | 84.5        84  with 1/1 error, 30 M steps, best trained on full data till Dec2017
#2.5e-3 94.1        90.8 | 82.7      77.7  with 4/1 error, batch 100, 40 M steps <============= best trained on full data till Feb 19
#2.5e-3 93          90   | 81          71  with 4/1 error, batch 100, 300k steps, after a 54_40 nonML start, and drop50
#2.5e-3                  | 84.4  81.8 81.8 only p2v on full data, starting from 40M fitted 4:1. Separate fit is better than combined nm+p2v!    But does it mean extrapolation is better????                                  
# 3e-3  92.3  93    52.5 | 85.1  81.3  19  <==== looks best cross-val 
# 4e-3  91    90    37.1 | 84.6  80.6  12
# 6e-3  89    85     9   | 83.6  78.3 -43
# 8e-3  87.6  84.1   2   |
########################################################################
# Are we getting any closer to improvement?
# Cross-val on pre-ML data and on pre-ML+2017 data to predict the 2018 Jan ML data
# 400k steps, droput 75, 100 batch
########################################################################
#            nm          |    p2v           
#2.5e-3 4:1  93.6  71.7  | 81.2 -50.5   pre-2017 - an unlucky example with bad initialization
#2.5e-3 4:1  93.6  50    | 81.4  40     pre-2017 - better example with good initialization
#2.5e-3 4:1  93.6  64    | 81.4  20     pre-2017 - better example with good initialization
#2.5e-3 4:1  93.4  86.9  | 81.9  47.7   2017  Oh, yes? - mabe unlucky start too and could be even better   
#2.5e-3 4:1  94.2  91    | 82.6  50     2017 overtrained (3M steps), does not diverge
########################################################################
# Repeat what Misha did. non-ML as training, ML 2017+2018 for testing
########################################################################
# strongly depends on run
# 62 31 testing!!!!  (94 80 training)
# 22 16   (94 82)
# 51 -64  (94 81) and is getting worse and worse -200
# how to get that perfect start again???? and how to know I've hit it based on non-ML cross-val???
# so, refit non-ML to find the best one on ML test, save, continue fitting ML starting from this one (careful with min-max changed!!! merge them AFTER rescaling)
########################################################################
# OR fit nm and p2v separately (to avoid rescaling p2v for L2)
# is R**2 for both dimensions causing more curviness? or does combined NM+P2V error improve regularization compared to NM and P2V fit separately?
# add auxilliary function for better extrapolation
# too slow, get a GPU
########################################################################
# April 20, 2018 
# cleaned the bad 1100-1200nm p2v, corrected manually to 7, 
# start from preML fit 64 20
# 500k steps: full data 93.3  81.4, ML data only  88.89  73
########################################################################
# May 5, added James's Mar-Apr data, start from preML fit 64 20
########################################################################
# 800k steps:               93.5  82.2   |       90.8   78.2   - best so far
# soft i.e. L2*2 :          92.3  79.8   |       86.5   69 
########################################################################
# May 11
# everything above is wrong (keep-prob was not activated)
# i.e. it is trained completely without dropout
# without dropout, could get the same results with L2 with just 7 or 5? nodes
########################################################################
# noL2, Drop5%,             93    82     |       88     78     - lines are terrible! i.e. these R2s are pretty much the limit  
# 2.5e-4, drop 5%, 5M steps 89.6  76.3   |       78.6   65.5   - lines are not smooth still
# 2.5e-3, nodrop   1M       93.89 82.57  |       89.8   78.25  - still too cruvy
#   5e-3, nodrop   1M       92.3  79.5   |       85.3   69.5   - lines are ok, tails are still too curvy
#   5e-3, drop 1%  1M       89.5  73     |       73.6   50     - lines are smooth, tails too curvy, fit not great 
#   5e-3, drop 5%  1M       82.5  56     |       46.8    1.8   - smoothness good (tails still a bit funky), fit pretty bad
# 2.5e-3, drop 5%  1M       85.8  62     |       57     24     - smooth, but tails are funky, fit quite bad
########################################################################
# 2*5 nodes, not faster at all than 2*20 nodes (8 cursor flashes with batch 100), 300k steps
########################################################################
#  5e-3, drop 0             83.5  60.5   |       48.9   13.1   - smooth lines and tails, bad fit

########################################################################
# July 25, added May-Jun-Jul data, start from preML fit 64 20
########################################################################
# 2.5e-3, drop 0   100k     92.5  80.6   |       85.5   71.9   - lines ok, tails maybe a bit weird
########################################################################









########################################################################
# Intitialization
########################################################################
def weight_variable(shape):
  initial = tf.truncated_normal(shape, stddev=0.1) 
  #initial = tf.random_uniform(shape, minval=-0.01, maxval=0.01)
  return tf.Variable(initial)

def bias_variable(shape):
  initial = tf.constant(0.3, shape=shape)
  return tf.Variable(initial)

def get_random_indices(min_, max_, num):
   I = random.sample(range(min_, max_), num);
   return I;

   
   
   
   
   
def read_data():
  f    = open("full.dat", "r");
  test = open("lines.dat", "r");
  x = [];
  y = [];
  testDataX = [];
  testDataY = [];

  for line in f:  ### read TRAINING data #########################################################
    val = line.split();
    #               t_env            Pb                ODE              OLA               tmax             TMS                 ODE               Cl                  Cl
    inp = [(float)(val[0]), (float)(val[1]),  (float)(val[2]),  (float)(val[3]),  (float)(val[4]),  (float)(val[5]),  (float)(val[6]),  (float)(val[7]),  (float)(val[8])];
    E = 1240/(float)(val[9])
    peak2valley = (float)(val[10]) ######################
    out = [E, (0.16*np.exp(-peak2valley/3.07)+0.04)]  
    #print ("Orig",inp,out);

    '''
    # balance output bins ----------------------------------------------------------------
    if ((float)(val[9])>500 and (float)(val[9])<800):
      Nbalance = 8;
    if ((float)(val[9])>800 and (float)(val[9])<900):
      Nbalance = 3;
    if ((float)(val[9])>900 and (float)(val[9])<950):
      Nbalance = 1;
    if ((float)(val[9])>1000 and (float)(val[9])<1800):
      Nbalance = 4;
    if ((float)(val[9])>1700 and (float)(val[9])<2500):
      Nbalance = 10;

    if ((float)(val[10])<1):  #override those bad points
      Nbalance = 1;
    '''
    Nbalance = 1
    for j in range(Nbalance):
      # non-ML data points
      if( ((float)(val[1])==18) or ((float)(val[1])==4.5) or ((float)(val[1])==7) or ((float)(val[1])==19)):
        #if(0==0):  # do not use, training has to remain preML, so that ranges don't change!
        # note that ML data is added to training later on
        x.append(inp);
        y.append(out);

    '''
    #++ AUGMENTING Cl->noCl data ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Cl_amount = (float)(val[7])+(float)(val[8])
    if(Cl_amount>0.24 and Cl_amount<0.41):
      inp = [(float)(val[0]), (float)(val[1]),  (float)(val[2]),  (float)(val[3]),  (float)(val[4]),  (float)(val[5]),  (float)(val[6]),  0,  0];
      E = 1240/(float)(val[9])
      peak2valley = (float)(val[10]) / 1.3 #################
      out = [E, (0.16*np.exp(-peak2valley/3.07)+0.04)]  
      #print ("Augm1",inp,out);
      
      for j in range(Nbalance):
        x.append(inp);
        y.append(out);
      
    #++ AUGMENTING noCl->Cl data ++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    if(Cl_amount==0):
      inp = [(float)(val[0]), (float)(val[1]),  (float)(val[2]),  (float)(val[3]),  (float)(val[4]),  (float)(val[5]),  (float)(val[6]),  0.,  0.3];
      E = 1240/(float)(val[9])
      peak2valley = (float)(val[10]) * 1.3 ######################
      out = [E, (0.16*np.exp(-peak2valley/3.07)+0.04)]  
      #print ("Augm2",inp,out);
      
      for j in range(Nbalance):
        x.append(inp);
        y.append(out);
    '''

  X = np.array(x);
  Y = np.array(y);

  max_x = [np.max(X[:,0]), np.max(X[:,1]), np.max(X[:,2]), np.max(X[:,3]), np.max(X[:,4]), np.max(X[:,5]), np.max(X[:,6]), np.max(X[:,7]), np.max(X[:,8])]
  min_x = [np.min(X[:,0]), np.min(X[:,1]), np.min(X[:,2]), np.min(X[:,3]), np.min(X[:,4]), np.min(X[:,5]), np.min(X[:,6]), np.min(X[:,7]), np.min(X[:,8])]

  max_y = [np.max(Y[:,0]), np.max(Y[:,1])]
  min_y = [np.min(Y[:,0]), np.min(Y[:,1])]

  
  
  
  
  
  
  
  
  
 

  for line in test:   #### read TESTING data ########################################################
    val = line.split();

    # Most popular recipe for 950nm:  # don't forget to shift both inp and append below
    # 4.5      15  0  125-127-130C  180 / 10     0  0-0.25-0.33
    #if(((float)(val[1])==4.5) and  ((float)(val[2])==15) and  ((float)(val[3])==0) and  ((float)(val[4])>129)  and  ((float)(val[4])<131) and  ((float)(val[5])==180)  and  ((float)(val[6])==10)                                  and  ((float)(val[7])==0) and ((float)(val[8])<0.01) ):
    #if(((float)(val[1])==4.5) and  ((float)(val[2])==15) and  ((float)(val[3])==0) and  ((float)(val[4])>125)  and  ((float)(val[4])<135) and  ((float)(val[5])==180)    and ((float)(val[6])>7) and ((float)(val[6])<13)           and  ((float)(val[7])==0) and ((float)(val[8])<0.01) ):

    # Most popular recipe for 1100-1300nm:
    # 18      15  0  100-110C  240 / 8-10
    #if(((float)(val[1])==18) and    ((float)(val[2])==15) and    ((float)(val[3])==0) and        ((float)(val[4])>99)  and  ((float)(val[4])<111)         and  ((float)(val[5])>239)  and  ((float)(val[5])<241)     and  ((float)(val[6])>7)  and  ((float)(val[6])<11)     and  ((float)(val[7])==0)      and ((float)(val[8])<0.01) ):

    # Most popular recipe for 1550nm:
    # 18      15  0  120-130C  190-210 / 8-12
    #if(((float)(val[1])==18) and    ((float)(val[2])==15) and    ((float)(val[3])==0) and      ((float)(val[4])>119)  and  ((float)(val[4])<131)       and  ((float)(val[5])>189)  and  ((float)(val[5])<211)     and  ((float)(val[6])>7)  and  ((float)(val[6])<13)     and  ((float)(val[7])==0)      and ((float)(val[8])<0.01) ):

    # include all 2017 and beyond ML-driven data points in testing
    if( ((float)(val[1])!=18) and ((float)(val[1])!=4.5) and ((float)(val[1])!=7)  and ((float)(val[1])!=19)):
    #if(0==0): 

    # data with Cl
    #if( (float)(val[7])>0 or (float)(val[8])>0 ):

      inp = [(float)(val[0]), (float)(val[1]),  (float)(val[2]),  (float)(val[3]),  (float)(val[4]),  (float)(val[5]),  (float)(val[6]),  (float)(val[7]),  (float)(val[8])];
      E = 1240/(float)(val[9])
      peak2valley = (float)(val[10]) ######################
      out = [E, (0.16*np.exp(-peak2valley/3.07)+0.04)]  

      testDataX.append(inp);
      testDataY.append(out);

      ##############################################################
      #now can append ML data to training, because  min and max has been already determined above
      x.append(inp);
      y.append(out);
 

 
  testX = np.array(testDataX);
  testY = np.array(testDataY);
     
  #reassign training after ML data has been added
  #cannot just have it here because min-max requires it as array
  X = np.array(x);
  Y = np.array(y);

  '''
  file = open("SigOpt_MLonly.txt","w")
  file.write(str(testDataX))
  file.write(str(testDataY))
  file.close()
  '''
  
  
  
  
  
  
  
  
  # RESCALING ###########################################################

  
  #training-------------------------------------------------------
  print ('Training size:',len(X));
  for i in range(len(X)):
    for j in range(9):

      if (j==0):
        suppress = 50;   #t_env
      elif (j==1):
        suppress = 1;      #pb
      elif (j==2):
        suppress = 1;      #ode
      elif (j==3):
        suppress = 1;      #ola
      elif (j==4):
        suppress = 1;      #t_max
      elif (j==5):
        suppress = 1;      #tms
      elif (j==6):
        suppress = 1;      #ode
      elif (j==7):
        suppress = 1;      #cl
      else:
        suppress = 1;   #cl

      if (max_x[j] == min_x[j]):
        X[i,j] = 0;  # this is to exclude the variable from fitting
      else:
        X[i,j] = (X[i,j] - min_x[j]) / (max_x[j] - min_x[j]) / suppress ;

    for j in range(2):
      Y[i,j] = (Y[i,j] - min_y[j]) / (max_y[j] - min_y[j]);
      if(j==1):   # rescale in case different L2 is needed
        Y[i,j] = Y[i,j]/1



  #testing-------------------------------------------------
  #print (testX[218,4]);
  for i in range(len(testX)):
    for j in range(9):

      if (j==0):
        suppress = 50;   #t_env
      elif (j==1):
        suppress = 1;      #pb
      elif (j==2):
        suppress = 1;      #ode
      elif (j==3):
        suppress = 1;      #ola
      elif (j==4):
        suppress = 1;      #t_max
      elif (j==5):
        suppress = 1;      #tms
      elif (j==6):
        suppress = 1;      #ode
      elif (j==7):
        suppress = 1;      #cl
      else:
        suppress = 1;     #cl

      if (max_x[j] == min_x[j]):
        testX[i,j] = 0;  # this is to exclude the variable from fitting
      else:
        testX[i,j] = (testX[i,j] - min_x[j]) / (max_x[j] - min_x[j]) / suppress ;


    for j in range(2):
      testY[i,j] = (testY[i,j] - min_y[j]) / (max_y[j] - min_y[j]);
      if(j==1):  # rescale in case different L2 is needed
        testY[i,j] = testY[i,j]/1

    #print (i, testX[i,0],testX[i,1],testX[i,2],testX[i,3],testX[i,4],testX[i,5],testX[i,6],testX[i,7],testX[i,8],testY[i,0],testY[i,1]);

  print('X range:')
  print(min_x)
  print(max_x)
  print('Y range:')
  print(min_y)
  print(max_y)
  
  
    
  
  return X, Y, max_x, min_x, max_y, min_y, testX, testY;














########################################################################
# MODEL
########################################################################

import tensorflow as tf
import numpy as np
import random
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from mpl_toolkits.mplot3d import Axes3D

x_data, y_data, max_x, min_x, max_y, min_y, x_test, y_test = read_data();








keep_prob = tf.placeholder(tf.float32)
x  = tf.placeholder(tf.float32, shape=[None, 9])
y_ = tf.placeholder(tf.float32, shape=[None, 2])


W1 =  weight_variable([9, 20]) # linear
W2 =  weight_variable([20, 20]) # 1 layer
W3 =  weight_variable([20, 2]) # 2 layers
#W4 =  weight_variable([40, 2]) # 3 layers
#W5 =  weight_variable([32, 2])

b1 = bias_variable([20])  # linear
b2 = bias_variable([20])  # 1 layer
b3 = bias_variable([2])  # 2 layers
#b4 = bias_variable([2])  # 3 layers
#b5 = bias_variable([2])
#==============================================

#Create a saver that will save our data later
saver = tf.train.Saver()

# define our model
y2 = tf.nn.dropout(tf.nn.elu(tf.matmul(x, W1)+b1), keep_prob);  # 1st layer
y3 = tf.nn.dropout(tf.nn.elu(tf.matmul(y2,W2)+b2), keep_prob); # 2nd layer
#y2 = tf.nn.elu(tf.matmul(x, W1)+b1);  # 1st layer
#y3 = tf.nn.elu(tf.matmul(y2,W2)+b2); # 2nd layer

#y4 = tf.nn.dropout(tf.nn.elu(tf.matmul(y3,W3)+b3), keep_prob); # 3rd layer
#y5 = tf.nn.dropout(tf.nn.elu(tf.matmul(y4,W4)+b4), keep_prob);

#prediction  = tf.matmul(x,W1) + b1;  # linear regression
#prediction  = tf.matmul(y2,W2) + b2;  # 1 layer
prediction  = tf.matmul(y3,W3) + b3;  # 2 layers
#prediction  = tf.matmul(y4,W4) + b4;  # 3 layers
#prediction  = tf.matmul(y5,W5) + b5;  # 4 layers

#===============================================================================
# ERRORS
#===============================================================================
errorNM  = tf.reduce_mean((y_[:,0]-prediction[:,0])**2)
errorP2V = tf.reduce_mean((y_[:,1]-prediction[:,1])**2)
#errorPURE = tf.reduce_mean( 0.001*(y_[:,0]-prediction[:,0])**2  +  1*(y_[:,1]-prediction[:,1])**2 )   # cross-validation
errorPURE = tf.reduce_mean( 0.8*(y_[:,0]-prediction[:,0])**2  +  0.2*(y_[:,1]-prediction[:,1])**2 )   # balanced fit based on noise spread




NM_mean =  tf.reduce_mean(y_[:,0])
P2V_mean = tf.reduce_mean(y_[:,1])

NM_var  = tf.reduce_mean((y_[:,0] - NM_mean)**2)
P2V_var = tf.reduce_mean((y_[:,1] - P2V_mean)**2)

R2  = (1 - errorNM / NM_var ) * 100
R2_ = (1 - errorP2V / P2V_var ) * 100


'''
        tf.reduce_mean( tf.reduce_mean(W4**2, reduction_indices=[0]) )*10   +  \
        tf.reduce_mean( tf.reduce_mean(W3**2, reduction_indices=[0]) )*10   +  \
        tf.reduce_mean( tf.reduce_mean(W2**2, reduction_indices=[0]) )*10  +  \
        tf.reduce_mean( tf.reduce_mean(W1**2, reduction_indices=[0]) )*10
'''
        #tf.reduce_mean( tf.reduce_mean(W4**2, reduction_indices=[0]) )*3e-4   +  
error = errorPURE  +  \
        tf.reduce_mean( tf.reduce_mean(W3**2, reduction_indices=[0]) )*2.5e-3  +  \
        tf.reduce_mean( tf.reduce_mean(W2**2, reduction_indices=[0]) )*2.5e-3  +  \
        tf.reduce_mean( tf.reduce_mean(W1**2, reduction_indices=[0]) )*2.5e-3
        # do we need to include b in the L2???

train_step = tf.train.AdamOptimizer().minimize(error)
train_P2V = tf.train.AdamOptimizer().minimize(errorP2V)
train_NM = tf.train.AdamOptimizer().minimize(errorNM)

























##################################################################
# training
##################################################################

sess = tf.InteractiveSession()
tf.global_variables_initializer().run()

#saver.restore(sess, "saves/preML_64_20/model.ckpt")
saver.restore(sess, "saves/postML_64_20_Jul25/model.ckpt")

'''
# Cross-validation subset ##################################################################
dictionary = dict([(i,0) for i in range(len(x_data))])
I =   set(get_random_indices(0, len(x_data), (int)(0.2*len(x_data)) ));
ALL = set(get_random_indices(0, len(x_data), len(x_data)));
# don't forget to chose the non-ML data when reading from file
x_val = np.array([x_data[j] for j in I]);
y_val = np.array([y_data[j] for j in I]);
x_train =   np.array([x_data[j] for j in (ALL-I)]);
y_train =   np.array([y_data[j] for j in (ALL-I)]);
'''
# Use original manually split data and test instead
x_train = x_data
y_train = y_data
x_val = x_test
y_val = y_test

print ('training, validation, testing set length:',len(x_train),len(x_val),len(x_test))  




'''
# Gradient descent #####################################################################  
dictionary = dict([(i,0) for i in range(len(x_train))])
for i in range(100001):
  I = get_random_indices(0, len(x_train), 100);
  x_batch = np.array([x_train[j] for j in I]);
  y_batch = np.array([y_train[j] for j in I]);
  for k in I:
    dictionary[k]+=1
  #print I, x_batch, y_batch)
  #random.sample(group_of_items, num_to_select)
  sess.run(train_step, feed_dict={x: x_batch, y_: y_batch, keep_prob: 1}) #keep_prob is the probability of keeping the value
  #sess.run(train_P2V, feed_dict={x: x_batch, y_: y_batch, keep_prob: 1}) #keep_prob is the probability of keeping the value  
  #sess.run(train_NM, feed_dict={x: x_batch, y_: y_batch, keep_prob: 1}) #keep_prob is the probability of keeping the value  

  if(i%5000==0):
    #print(dictionary)
    print(i, \
          "Tnm",  sess.run(R2,        feed_dict={x: x_train, y_: y_train, keep_prob: 1}), \
          "Tpk",  sess.run(R2_,       feed_dict={x: x_train, y_: y_train, keep_prob: 1}), \
          #"L2",  sess.run(error/errorPURE-1, feed_dict={x: x_train, y_: y_train, keep_prob: 1}), \
       "   Vnm", sess.run(R2,        feed_dict={x: x_val, y_: y_val, keep_prob: 1}), \
          "Vpk", sess.run(R2_,       feed_dict={x: x_val, y_: y_val, keep_prob: 1}), \
          #"er", sess.run(errorPURE, feed_dict={x: x_test, y_: y_test, keep_prob: 1}) \
       "   Tnm", sess.run(R2,        feed_dict={x: x_test, y_: y_test, keep_prob: 1}), \
          "Tpk", sess.run(R2_,       feed_dict={x: x_test, y_: y_test, keep_prob: 1}),\
          );

  if(i%50000==0):
    save_path = saver.save(sess, "saves/postML_64_20_Jul25/model.ckpt")

  #if(sess.run(R2, feed_dict={x: x_data, y_: y_data, keep_prob: 1}) > 91.):
  #  i=900000
'''


print('Final:', \
          "R2nm",   sess.run(R2,        feed_dict={x: x_data, y_: y_data, keep_prob: 1}), \
          "R2peak", sess.run(R2_,       feed_dict={x: x_data, y_: y_data, keep_prob: 1}),\
          "nm",     sess.run(R2,        feed_dict={x: x_test[:,:], y_: y_test[:,:], keep_prob: 1}), \
          "R2peak", sess.run(R2_,       feed_dict={x: x_test[:,:], y_: y_test[:,:], keep_prob: 1}) \
         );



 
 



##########################################################################
# Plot training and testing data graphs
###########################################################################

Y_pred = (sess.run(prediction, feed_dict={x: x_train, y_: y_train, keep_prob: 1}))
plt.plot(y_train[:,0], y_train[:,1], 'or', Y_pred[:,0], Y_pred[:,1], 'ob', alpha=0.2)
#plt.plot(y_data[:,0], y_data[:,1], 'or', y_data[:,0], Y_pred[:,1], 'ob', alpha=0.2)
#plt.show()

Y_pred = (sess.run(prediction, feed_dict={x: x_val, y_: y_val, keep_prob: 1}))
plt.plot(y_val[:,0], y_val[:,1], 'oy', Y_pred[:,0], Y_pred[:,1], 'og')


#Y_pred = (sess.run(prediction, feed_dict={x: x_test, y_: y_data, keep_prob: 1}))
#plt.plot(y_test[:,0], y_test[:,1], 'oy', Y_pred[:,0], Y_pred[:,1], '.g')
##plt.plot(y_test[:,0], y_test[:,1], 'oy', y_test[:,0], Y_pred[:,1], '.g')
plt.show()



############ OUT vs IN ############################
'''
Y_pred = (sess.run(prediction, feed_dict={x: x_data, y_: y_data, keep_prob: 1}))
plt.plot(y_data[:,0], Y_pred[:,0], '.r')
plt.plot(y_data[:,1], Y_pred[:,1], '.b')
plt.show()

Y_pred = (sess.run(prediction, feed_dict={x: x_test, y_: y_data, keep_prob: 1}))
plt.plot(y_test[:,0], Y_pred[:,0], '.y')
plt.plot(y_test[:,1], Y_pred[:,1], '.g')

plt.show()
'''


'''
#CdCl effect
plt.plot(x_data[:,5], y_data[:,1], '.g')
plt.show()

plt.plot(x_data[:,6], y_data[:,1], '.g')
plt.show()
'''




#raise SystemExit(0)
















##############################################################################
# Scan lines along one dimension
##############################################################################

color =    ['blue','black','white','cyan','magenta','yellow','grey','green','green']
errorbar = [0.002,  0.06,   0.014,  0.002,   0.05,   0.036,  0.014,  0.016,  0.032 ]

print(x_test)
for k in range(len(x_test)) :

  plt.plot(y_data[:,0], y_data[:,1], '.r')
  plt.plot(y_test[:,0], y_test[:,1], '.', markerfacecolor='pink', markeredgecolor='pink')  

  for j in range(9):     #inputs
    x_lines = [];
    y_lines = [];

    for i in range(100): # points along the line
      x_point = list(x_test[k,:])
      x_point[j] = i/100*2   # full range
      #x_point[j] = x_point[j] + (0.5 - i/100) * errorbar[j] *2   # error bar
      y_point = [0, 0]

      x_lines.append(x_point);
      y_lines.append(y_point);

    x_lines = np.array(x_lines);
    y_lines = np.array(y_lines);

    Y_lines = (sess.run(prediction, feed_dict={x: x_lines, y_: y_lines, keep_prob: 1}))
    plt.plot(Y_lines[:,0], Y_lines[:,1], 'o', color=color[j] )
    
  plt.show()











#########################################################################
#
# Print the worst-fit points in order to repeat them experimentally
#
#########################################################################
'''
print('Calculating the worst points')

x_worst = x_test
y_worst = y_test
#use test_data which actually read the full data, but did not repeat it for balancing and not augment it
Y_pred = sess.run(prediction, feed_dict={x: x_test, y_: y_test, keep_prob: 1})




file = open("worst-fit points.txt","w")

for i in range(len(x_worst)):
  # if wavelength is very wrong
  if(abs(y_worst[i,0]-Y_pred[i,0])>0.2):
    print (i,y_worst[i,0],y_worst[i,1])  
    #scale back
    for j in range(9):
      x_worst[i,j] = x_worst[i,j] * (max_x[j] - min_x[j]) + min_x[j];
    for j in range(2):
      y_worst[i,j] = y_worst[i,j] * (max_y[j] - min_y[j]) + min_y[j];
    #convert to human-readable
    y_worst[i,0] = 1240/y_worst[i,0];
    y_worst[i,1] = y_worst[i,1]/80*(y_worst[i,0]-450)   *2

    file.write(str(x_worst[i,:])+str(y_worst[i,:])+'\n'.format('f'))
file.close()


'''












############################################################################################
#
# Predictions
#
############################################################################################

print('Generating predictions')

x_pred = [];
x1_pred = [];
x3_pred = [];
y_pred = [];

for i in range(600000):
   #full range ============== t_env ===================== ss2                               ODE                         OLA                                           t                          TMS                          ODE                         Cl                      Cl
   #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(-0.31,1.5),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.5),                np.random.uniform(-0.1,1.1),      np.random.uniform(-0.217,1.5), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.2), np.random.uniform(0,0.2)];   
   #a_pred = [np.random.uniform(0.9/50,0.9/50), np.random.uniform(0.,1.),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,1),                np.random.uniform(0,1),      np.random.uniform(0.,1.), np.random.uniform(0.,1.), np.random.uniform(0,0.), np.random.uniform(0,0.)];      

   #double range
   a_pred = [np.random.uniform(0.9/50,0.9/50), np.random.uniform(0.8,1.5),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.15),                np.random.uniform(0.,1),      np.random.uniform(+0.217,2), np.random.uniform(-0.1,1.1), np.random.uniform(0.0,0.0), np.random.uniform(0.05,0.1)];         
   '''
   # Pb:S = const 
   pb = np.random.uniform(0.+0.31,2.5+0.31)
   #pb = np.random.uniform(0.7+0.31,1.3+0.31)
   s  = np.random.uniform(-0.117+0.217,1.5+0.217)
   #s = (pb + np.random.uniform(-0.4,0.4))/ 1.87 + 0.217  # with OLA
   #s = (pb + np.random.uniform(-0.5,0.5))/ 0.7 + 0.217  # with OLA
   #s = (pb + np.random.uniform(-0.1,0.1)) /  1.75    # without OLA
   
   # 600nm
   #a_pred = [np.random.uniform(0.3/50,0.3/50), pb - 0.31,                    np.random.uniform(0.273,0.273),   np.random.uniform(0.5,0.7),                np.random.uniform(-0.1,0.1),      s-0.217,                       np.random.uniform(-0.1,1.1), np.random.uniform(0,0.), np.random.uniform(0,0.)];         
   # full range
   #a_pred = [0.3/50,                                 pb - 0.31,              np.random.uniform(0.273,0.273),   np.random.uniform(0.,0.2),                np.random.uniform(-0.1,1.1),      s-0.217,                       np.random.uniform(-0.1,1.1), np.random.uniform(0,0.), np.random.uniform(0,0.)];            
   a_pred = [0.9/50,                                  pb - 0.31,                          0.273,                np.random.uniform(0.,0.1),                np.random.uniform(0.2,0.8),      s-0.217,                      np.random.uniform(0.,1), np.random.uniform(0,0.), np.random.uniform(0,0.1)];      
   
   #optimal for 600nm, OLA 
   #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(0.45,1.2),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.2),                np.random.uniform(-0.05,0.25),      np.random.uniform(0.8,1.15), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.2), np.random.uniform(0,0.2)];   
   
   #optimal for 950nm, Pb 10, no OLA, no Cd
   #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(0.3,0.5),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.),                np.random.uniform(0.35,0.6),      np.random.uniform(0.4,0.6), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.), np.random.uniform(0,0.)];      
   #optimal for 950nm, Pb 18, OLA
   #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(0.8,1.),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.3),                np.random.uniform(0.5,0.7),      np.random.uniform(0.4,0.6), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.15), np.random.uniform(0,0.15)];
   #optimal for 950nm, Pb 11, OLA
   #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(0.4,0.6),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.3),                np.random.uniform(0.4,0.8),      np.random.uniform(0.2,0.4), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.15), np.random.uniform(0,0.15)];   
   
   #optimal for 1170nm, Pb 10, no OLA, no Cd
   #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(0.6,0.9),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.),                np.random.uniform(0.5,0.65),      np.random.uniform(0.35,0.65), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.), np.random.uniform(0,0.)];      
   #optimal for 1500nm, noOLA
   #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(1.1,1.6),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.),                np.random.uniform(0.5,0.75),      np.random.uniform(0.65,0.95), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.), np.random.uniform(0,0.)];      
   #optimal for 2200nm, noOLA   
   #a_pred = [np.random.uniform(0.3/50,0.3/50), np.random.uniform(2.2,3),   np.random.uniform(0.273,0.273),   np.random.uniform(0.0,0.),                np.random.uniform(0.2,0.7),      np.random.uniform(1.1,1.5), np.random.uniform(-0.1,1.1), np.random.uniform(0,0.), np.random.uniform(0,0.)];   
   '''
   x_pred.append(a_pred);


   #same points with only one parameter changed ====================================
   b_pred = list(a_pred)
   d_pred = list(a_pred)
   # CdCl
   '''
   b_pred[7] = np.random.uniform(0.,0.) #blue
   b_pred[8] = np.random.uniform(0.05,0.05)
   d_pred[7] = np.random.uniform(0.,0.)  #green
   d_pred[8] = np.random.uniform(0.05,0.05)
   
   #season
   b_pred[0] = np.random.uniform(0.9/50,0.9/50)  #blue
   d_pred[0] = np.random.uniform(0.9/50,0.9/50)  #green
   '''
   #OLA
   b_pred[3] = np.random.uniform(0.0,0.1) #blue, used for final predictions
   d_pred[3] = np.random.uniform(0.,0.)  #green
   
   #18 vs 4.5
   #b_pred[1] = np.random.uniform(0.,1)
   
   #ODE
   #b_pred[3] = np.random.uniform(0.35,0.65)

   x1_pred.append(b_pred);  #blue
   x3_pred.append(d_pred);  #green


   #placeholder for results ========================================================
   c_pred = [0, 0]
   y_pred.append(c_pred);

x_pred  = np.array(x_pred);
x1_pred = np.array(x1_pred);
x3_pred = np.array(x3_pred);
y_pred  = np.array(y_pred);

print('X range:')
print(min_x)
print(max_x)
print('Y range:')
print(min_y)
print(max_y)

#full range
Y_pred = (sess.run(prediction, feed_dict={x: x_pred, y_: y_pred, keep_prob: 1}))
plt.plot(Y_pred[:,0], Y_pred[:,1], '.', markerfacecolor='grey', markeredgecolor='grey', alpha=0.1)

#OLA, no Cd, one season
Y1_pred = (sess.run(prediction, feed_dict={x: x1_pred, y_: y_pred, keep_prob: 1}))
plt.plot(Y1_pred[:,0], Y1_pred[:,1], '.b', alpha=0.1)

#no OLA, no Cd, one season
Y3_pred = (sess.run(prediction, feed_dict={x: x3_pred, y_: y_pred, keep_prob: 1}))
plt.plot(Y3_pred[:,0], Y3_pred[:,1], '.g', alpha=0.1)

#original data
plt.plot(y_data[:,0], y_data[:,1], '.r')  # all
plt.plot(y_test[:,0], y_test[:,1], '.', markerfacecolor='pink', markeredgecolor='pink')  # with Cl



'''
#test predictions (for Teds presentation)
Y_pred = (sess.run(prediction, feed_dict={x: x_data, y_: y_data, keep_prob: 1}))
plt.plot(Y_pred[:,0], Y_pred[:,1], '.k')
Y_pred = (sess.run(prediction, feed_dict={x: x_test, y_: y_data, keep_prob: 1}))
plt.plot(Y_pred[:,0], Y_pred[:,1], '.k')
#careful, they screw up Y_pred used later for reverse data!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
'''

plt.show()



#########################################################################
# UNCERTAINTIES
#########################################################################
'''
for i in range(200):
  Y_pred = (sess.run(prediction, feed_dict={x: x_pred, y_: y_pred, keep_prob: 0.99}))
  plt.plot(Y_pred[:,0], Y_pred[:,1], '.b')

Y_pred = (sess.run(prediction, feed_dict={x: x_pred, y_: y_pred, keep_prob: 1}))
plt.plot(Y_pred[:,0], Y_pred[:,1], 'or')

plt.show()
'''



#x_pred = x_test # for sigopt
#x_pred = x_data  # original data
##########################################################################
# Plot inputs distribution
###########################################################################
'''

Y_pred = (sess.run(prediction, feed_dict={x: x_pred, y_: y_pred, keep_prob: 1}))   # full range predictions

# define how close the point is to optimum
size=[]
wavelength=[]
p2v=[]
for i in range(len(x_pred)) :
  #size.append (  0.8*(Y_pred[i,0]-0.527)**2 + 0.2* (1e-1)**2  ) # only wavelength
  #size.append (  0.8*(Y_pred[i,0]-1.035)**2  +  0.2*(Y_pred[i,1]-0.6)**2  )    # both, 600 nm     
  #size.append (  0.8*(Y_pred[i,0]-0.527)**2  +  0.2*(Y_pred[i,1]-0.225)**2  )   # both, 950 nm 
  #size.append (  Y_pred[i,1] )   # p2v 950 nm   
  #size.append (  0.8*(Y_pred[i,0]-0.427)**2  +  0.2*(Y_pred[i,1]-0.043)**2  )   # both, 1070 nm   
  #size.append (  0.8*(Y_pred[i,0]-0.359)**2  +  0.2*(Y_pred[i,1]-0.004)**2  )   # both, 1170 nm     
  #size.append (  0.8*(Y_pred[i,0]-0.199)**2  +  0.2*(Y_pred[i,1]-(-0.025))**2  )# both, 1500nm
  #size.append (  0.8*(Y_pred[i,0]-0.)**2      +  0.2*(Y_pred[i,1]-0.318)**2  )   # both, 2200nm    
  
  if abs(Y_pred[i,0]-0.2)>0.01:  # slice through
    size.append (  0  )
  if abs(Y_pred[i,0]-0.2)<=0.01:
    size.append (  Y_pred[i,1]  )

  wavelength.append(Y_pred[i,0])
  p2v.append(Y_pred[i,1])
  #wavelength.append(0.527) #950nm
  
size = np.array(size)
p2v=np.array(p2v)
wavelength=np.array(wavelength)




# 2D plot Pb vs S plane, deviation from optimum ==================================================================================
#plt.scatter(x_pred[:,1],x_pred[:,5], s=(1/size)**2/30000, c=1/size)  # deviation
plt.scatter(x_pred[:,1],x_pred[:,5], s=size*10, c=size)               # p2v for a slice
#plt.scatter(x_pred[:,1],x_pred[:,4], s=(1/size), c=1/size)           # abs value
plt.xlabel("Pb")
plt.ylabel("S")
plt.show() 
'''


'''
# 2D plot Pb vs S plane, wavelength
plt.scatter(x_pred[:,1],x_pred[:,5], s=wavelength, c=wavelength, cmap=cm.nipy_spectral)  # wavelength
plt.colorbar()
plt.xlabel("Pb")
plt.ylabel("S")
plt.show() 

# 2D plot Pb vs S plane, peak2valley
plt.scatter(x_pred[:,1],x_pred[:,5], s=p2v, c=p2v)  # p2v
plt.colorbar()
plt.xlabel("Pb")
plt.ylabel("S")
plt.show() 
'''

'''
# 3D ==================================================================================
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(x_pred[:,1], x_pred[:,5],  x_pred[:,4], zdir='z', s=(1/size)**2/30000, c=1/size, depthshade=False)
ax.set_xlabel('Pb')
ax.set_ylabel('S')
ax.set_zlabel('t_max')
plt.show() 
'''




# 2D along Pb-S line vs t ==================================================================================
'''
#plt.scatter((x_pred[:,1]+0.31)/(x_pred[:,5]+0.217),x_pred[:,4], s=(1/size)**2/30000, c=1/size)  
plt.scatter((x_pred[:,1]+0.31)/(x_pred[:,5]+0.217),x_pred[:,4], s=size*10, c=size)  # slice
plt.xlabel("Pb/S")
plt.ylabel("t")
plt.show() 
'''


'''
# 2D along Pb-S line vs OLA
plt.scatter((x_pred[:,1]+0.31)/(x_pred[:,5]+0.217),x_pred[:,3], s=(1/size)**2/30000, c=1/size)  
plt.xlabel("Pb/S")
plt.ylabel("OLA")
plt.show() 


# 2D plot Pb/S vs t, wavelength
plt.scatter((x_pred[:,1]+0.31)/(x_pred[:,5]+0.217),x_pred[:,4], s=wavelength, c=wavelength, cmap=cm.nipy_spectral)  # wavelength t
plt.colorbar()
plt.xlabel("Pb/S")
plt.ylabel("t")
plt.show() 

# 2D plot Pb/S vs t, p2v
plt.scatter((x_pred[:,1]+0.31)/(x_pred[:,5]+0.217),x_pred[:,4], s=p2v, c=p2v, cmap=cm.nipy_spectral)  # p2v t
plt.colorbar()
plt.xlabel("Pb/S")
plt.ylabel("t")
plt.show() 
'''
'''
# 2D plot Pb/S vs OLA plane, wavelength
plt.scatter((x_pred[:,1]+0.31)/(x_pred[:,5]+0.217),x_pred[:,3], s=wavelength, c=wavelength)  # wavelength OLA
plt.colorbar()
plt.xlabel("Pb/S")
plt.ylabel("OLA")
plt.show() 
'''







#exit()

#x1_pred = x_pred  # for sigopt
#x1_pred = x_test  # for James
#########################################################################
#
# Scale predictions back
#
#########################################################################
print('Scaling predictions back ')

for i in range(len(x1_pred)):  # x1 = b = blue = OLA
    for j in range(9):
      if(j==0):
        x1_pred[i,j]=x1_pred[i,j]*50  
      x1_pred[i,j] = x1_pred[i,j] * (max_x[j] - min_x[j]) + min_x[j];
    Y1_pred[i,0] =    Y1_pred[i,0] * (max_y[0] - min_y[0]) + min_y[0];
    Y1_pred[i,1] = 1* Y1_pred[i,1] * (max_y[1] - min_y[1]) + min_y[1];

    #Y1 = FWHM/E = 0.16*np.exp(-peak2valley/3.07)+0.04
    #p2v= -ln[(Y1-0.04)/ 0.16]*3.07
    Y1_pred[i,0] = 1240/Y1_pred[i,0];
    Y1_pred[i,1] = -np.log((Y1_pred[i,1]-0.04)/ 0.16)*3.07

plt.plot(Y1_pred[:,0], Y1_pred[:,1], '.b', alpha=0.1)




print('Showing the red experimental data rescaled back')
for i in range(len(x_train)):
    for j in range(9):
      #if(j==0):
      #  x_train[i,j]=x_train[i,j]*50  # no need to scale here because it is not plotted
      x_train[i,j] = x_train[i,j] * (max_x[j] - min_x[j]) + min_x[j];
  
    y_train[i,0] =    y_train[i,0] * (max_y[0] - min_y[0]) + min_y[0];
    y_train[i,1] = 1* y_train[i,1] * (max_y[1] - min_y[1]) + min_y[1];

    #Y1 = FWHM/E = 0.16*np.exp(-peak2valley/3.07)+0.04
    #p2v= -ln[(Y1-0.04)/ 0.16]*3.07
    y_train[i,0] = 1240/y_train[i,0];
    y_train[i,1] = -np.log((y_train[i,1]-0.04)/ 0.16)*3.07

plt.plot(y_train[:,0], y_train[:,1], '.r')
plt.show()


########################################################################
#
# Show the best
#
########################################################################
print('Filtering to required range')

x_cut=[]
y_cut=[]

for i in range(len(x1_pred)):
  if((Y1_pred[i,0]>935) and (Y1_pred[i,0]<945) and (Y1_pred[i,1]>4.1)  and (Y1_pred[i,1]<40)):
    x_cut.append(x1_pred[i,:])
    y_cut.append(Y1_pred[i,:])

x_cut = np.array(x_cut);
y_cut = np.array(y_cut);

plt.plot(y_cut[:,0], y_cut[:,1], '.y')
plt.show()


##########################################################################

file = open("940nm Jul25.txt","w")
file.write(str(x_cut))
#f.write('%8.2f' % (str(x_cut))
file.write(str(y_cut))
file.close()



