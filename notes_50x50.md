# 50x50, scaling 1:3 - 3152 params
## Trained on old+half new (2315+237/2), test on new (237/2) split 0.046






# 100 x 100, scaling 1:3 - 11302 params
 # Drop 5%
 - L2 **3e-7 5.95 4.9** 10k, too squigly and still not good enough fit
 - L2 3e-7 5.9  4.8     10k
 - L2 3e-7 6  4.85->4.64 after 5k, batch 325   
 - L2 1e-6 5.8  4.7
 - L2 1e-6 5.75 4.6 batch 128
  
  
  
  
  
  
  
### Drop 10%, batch 1300
train, test
- L2 1e-6  5.7  4.6  10k - pretty bad, 10% is too much for 50x50  
- L2 1e-5  5.5  4    13k enough - Definitely did not cover 2000nm properly
 
  
 
# 100 x 100, scaling 1:2 - 11302 params
# Drop 10%
 - L2 1e-6 5.4  4.4 - bad

  
  
# 500 x 500, scaling 1:3 - 256,502 params
# Drop 5%, batch 650
 - L2 1e-6 5.8  4.57-4.53  after 20k - these numbers are WITH dropout and on small batches
 - L2 1e-6 6.2  4.63  same as above, but with 0 Drop for predictions 
